use std::{env, process};

use medusa::{ArgType, Handler};

pub fn echo(handler: &Handler, payload: String) {
    println!("got : {}", payload);
}

pub fn show_help(handler: &Handler) {
    let usage: &str = "
available commands :

    --help            show this help prompt
    --echo String     print some string String to show that the binary works
    --version         show current version of binary
";
    println!("{}", usage);

    // force exit
    process::exit(0);
}

pub fn get_version(handler: &Handler) {
    if let Ok(version) = env::var("CARGO_PKG_VERSION") {
        println!("{}", version);
    } else {
        println!("error : couldn't get version variable.");
        process::exit(1);
    }

    // force exit
    process::exit(0);
}

pub fn print_twice(handler: &Handler) {
    if let Some(argtype) = handler.get_arg("--echo") {
        if let ArgType::Content(payload) = argtype {
            println!("printed once more : {}", payload);
        }
    }
}
