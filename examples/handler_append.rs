use std::env;

use medusa::{CommandLine, Handler, Variant};

mod functions;

fn main() {
    // init first handler
    let mut handler: Handler = Handler::new();
    handler.add(
        String::from("--echo"),
        Variant::WithArg(functions::echo),
        String::from("Print the argument passed for this option."),
    );

    // then init second handler
    let mut other_handler: Handler = Handler::new();
    other_handler.add(
        String::from("--other-echo"),
        Variant::WithArg(functions::echo),
        String::from("Print the other argument passed for this other option."),
    );

    // and maybe init another handler again
    let mut another_handler: Handler = Handler::new();
    another_handler.add(
        String::from("--example-echo"),
        Variant::WithArg(functions::echo),
        String::from("Print the example argument passed for this example option."),
    );

    // append handler from the other handler
    handler.append(other_handler);

    let mut command: CommandLine = CommandLine::new();
    command.set_handler(another_handler);

    // or append handler from CLI
    command.append_handler(handler);

    command.set_name("appender");
    command.set_version("1.0.0");
    command.run(env::args());
}
