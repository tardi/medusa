use std::env;

use medusa::{CommandLine, Handler, Variant};

mod functions;

fn main() {
    let mut handler: Handler = Handler::new();
    handler.add(
        String::from("--help"),
        Variant::Plain(functions::show_help),
        String::from("Show this help message."),
    );
    handler.add(
        String::from("--echo"),
        Variant::WithArg(functions::echo),
        String::from("Print the argument passed for this option."),
    );
    handler.add(
        String::from("--version"),
        Variant::Plain(functions::get_version),
        String::from("Print current version."),
    );
    handler.add(
        String::from("--twice"),
        Variant::Plain(functions::print_twice),
        String::from("Print again the payload \"--echo\" have."),
    );

    let mut command: CommandLine = CommandLine::new();
    command.set_handler(handler);
    command.set_name("mycli");
    command.set_version("v1.2.8-alpha.1");
    command.run(env::args());
}
