//! # medusa
//!
//! `medusa` is a general template for building
//! command line interface (CLI) using Rust

mod core;

pub use crate::core::command_line::CommandLine;
pub use crate::core::handler::{ArgType, Handler, Variant};
