use std::collections::HashMap;

/// Enum that define the variants of the CLI option handlers.
///
/// There are 2 types of this variant: `Plain` and `WithArg`.
///
/// `Plain` means that your handler doesn't need arguments to run.
/// For example, a handler to show help message or to output current version.
///
/// `WithArg` means that your handler is a function that need arguments to run.
/// For example, a handler to use certain user to run the command.
///
/// Note that every functions that will be registered as action handler
/// must pass a variable with type `&Handler` as its first parameter.
/// This is required to make every action handler becomes _stateful_
/// (means that it can parse and use all of the arguments passed to the
/// CLI program).
///
/// # Example
///
/// ```rust
/// use medusa::Handler;
///
/// // without arguments (using `Plain` variant)
/// // the usage would be:
/// // Variant::Plain(hello);
/// fn hello(handler: &Handler) {
///   println!("Hello, world!");
/// }
///
/// // with arguments (using `WithArg` variant)
/// // the usage would be:
/// // Variant::WithArg(echo);
/// fn echo(handler: &Handler, payload: String) {
///   println!("Got payload : {}", payload);
/// }
/// ```
#[derive(Clone)]
pub enum Variant {
    Plain(fn(&Handler)),
    WithArg(fn(&Handler, String)),
}

/// Enum that define the type (or kind) of option arguments.
///
/// There are 2 types of this variant: `Flag` and `Content`.
///
/// `Flag` means that your arguments are only a variable that tells
/// if it is exist (`true`) or not (`false`), since it only contains
/// boolean value.
/// For example, option parameter `--wait` will make your CLI
/// program wait until the process are finished, otherwise it will
/// run as a background process. Here, option `--wait` will have
/// value of `true` because it is called when executing the program.
///
/// `Content` means that your arguments consist of a `String` instance
/// that can be processed later (hence the name, `Content`).
/// For example, option parameter `--user nobody` will make your CLI
/// program run as user `nobody`. Here, option `--user` will have
/// argument value of `nobody` since this argument is passed to the
/// option `--user`.
#[derive(Clone)]
pub enum ArgType {
    Flag(bool),
    Content(String),
}

/// Struct that defines a CLI option handler in a programmatic way.
#[derive(Clone)]
pub struct Handler {
    actions: Option<HashMap<String, Variant>>,
    hints: Option<HashMap<String, String>>,
    values: Option<HashMap<String, ArgType>>,
}

impl Handler {
    /// Create a new empty `Handler` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::Handler;
    ///
    /// // code ...
    ///
    /// let mut handler: Handler = Handler::new();
    /// ```
    pub fn new() -> Handler {
        Handler {
            actions: None,
            hints: None,
            values: None,
        }
    }

    /// Manually parse the options and arguments passed to the CLI program.
    ///
    /// This method **meant to be called internally** by `CommandLine` instance,
    /// and indeed it is currently being called internally.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::Handler;
    ///
    /// use std::env;
    ///
    /// // code ...
    ///
    /// let mut handler: Handler = Handler::new();
    /// let result = handler.parse_args(env::args().collect());
    /// println!("result : {:?}", result);
    /// ```
    pub fn parse_args(&mut self, args: Vec<String>) -> Result<(), String> {
        if let Some(map) = &self.actions {
            if let None = &mut self.values {
                let mut new_values: HashMap<String, ArgType> = HashMap::new();
                let mut option_key: Option<String> = None;
                let mut status: Result<(), String> = Ok(());

                let args = args.iter().skip(1);
                for option in args {
                    match map.get(option) {
                        None => {
                            if let Some(key) = &option_key {
                                new_values
                                    .insert(key.clone(), ArgType::Content(option.to_string()));
                                status = Ok(());
                            } else {
                                let mut msg = String::from("error parsing argument : ");
                                msg.push_str(&option.as_str());
                                return Err(msg);
                            }
                        }
                        Some(variant) => {
                            match variant {
                                Variant::Plain(_) => {
                                    new_values.insert(option.to_string(), ArgType::Flag(true));
                                }
                                Variant::WithArg(_) => {
                                    // save for further processing

                                    let mut msg = String::from("handler does not have value : ");
                                    msg.push_str(&option.as_str());
                                    status = Err(msg);

                                    option_key = Some(option.to_string());
                                }
                            }
                        }
                    }
                }

                // only set to struct if it contains minimum of single element
                if new_values.len() > 0 {
                    self.values = Some(new_values);
                }

                status
            } else {
                return Err(String::from("handler values not empty"));
            }
        } else {
            Err(String::from("no action handlers found"))
        }
    }

    /// Get the argument of CLI option as its `key`.
    ///
    /// This function can be called in the inner side of any
    /// action handler function definition to get the argument
    /// of the parameter option specified. The value return are
    /// the `ArgType` instance. See its documentation for details.
    /// See also the `stateful` example for code snippet.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{ArgType, Handler};
    ///
    /// // code ...
    ///
    /// fn print_twice(handler: &Handler) {
    ///     if let Some(argtype) = handler.get_arg("--echo") {
    ///         if let ArgType::Content(payload) = argtype {
    ///             println!("printed once more : {}", payload);
    ///         }
    ///     }
    /// }
    /// ```
    pub fn get_arg(&self, key: &str) -> Option<ArgType> {
        match &self.values {
            None => None,
            Some(value_map) => {
                if let Some(argtype) = value_map.get(key) {
                    return Some(argtype.clone());
                } else {
                    return None;
                }
            }
        }
    }

    /// Get action handler list in form of `HashMap` from the current
    /// `Handler` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.get_actions() {
    ///     println!("Got some actions here!");
    /// } else {
    ///     println!("No actions defined!");
    /// }
    /// ```
    pub fn get_actions(&self) -> Option<&HashMap<String, Variant>> {
        if let Some(map) = &self.actions {
            return Some(map);
        } else {
            return None;
        }
    }

    /// Clone action handler list in form of `HashMap` from the current
    /// `Handler` instance.
    ///
    /// Note that the return value from this method
    /// are a `HashMap` instance wrapped in `Option` instance, instead
    /// of _reference_ to a `HashMap` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.clone_actions() {
    ///     println!("Got some cloned actions here!");
    /// } else {
    ///     println!("No actions defined!");
    /// }
    /// ```
    pub fn clone_actions(&self) -> Option<HashMap<String, Variant>> {
        if let Some(map) = &self.actions {
            return Some((*map).clone());
        } else {
            return None;
        }
    }

    /// Consume `self` instance and return the `HashMap` containing
    /// consumed action handlers wrapped with `Option` instance.
    ///
    /// Note that the current instance are consumed, hence
    /// it won't be able to be used again unless it was cloned first.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.extract_actions() {
    ///     println!("Got some cloned actions here!");
    /// } else {
    ///     println!("No actions defined!");
    /// }
    /// ```
    pub fn extract_actions(self) -> Option<HashMap<String, Variant>> {
        if let Some(map) = self.actions {
            return Some(map);
        } else {
            return None;
        }
    }

    /// Get action hint list in form of `HashMap` from the current
    /// `Handler` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.get_hints() {
    ///     println!("Got some hints here!");
    /// } else {
    ///     println!("No hints defined!");
    /// }
    /// ```
    pub fn get_hints(&self) -> Option<&HashMap<String, String>> {
        if let Some(map) = &self.hints {
            return Some(map);
        } else {
            return None;
        }
    }

    /// Clone action hint list in form of `HashMap` from the current
    /// `Handler` instance.
    ///
    /// Note that the return value from this method
    /// are a `HashMap` instance wrapped in `Option` instance, instead
    /// of _reference_ to a `HashMap` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.clone_hints() {
    ///     println!("Got some cloned hints here!");
    /// } else {
    ///     println!("No hints defined!");
    /// }
    /// ```
    pub fn clone_hints(&self) -> Option<HashMap<String, String>> {
        if let Some(map) = &self.hints {
            return Some((*map).clone());
        } else {
            return None;
        }
    }

    /// Consume `self` instance and return the `HashMap` containing
    /// consumed action hints wrapped with `Option` instance.
    ///
    /// Note that the current instance are consumed, hence
    /// it won't be able to be used again unless it was cloned first.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// use std::collections::HashMap;
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// if let Some(map) = handler.extract_hints() {
    ///     println!("Got some cloned hints here!");
    /// } else {
    ///     println!("No hints defined!");
    /// }
    /// ```
    pub fn extract_hints(self) -> Option<HashMap<String, String>> {
        if let Some(map) = self.hints {
            return Some(map);
        } else {
            return None;
        }
    }

    /// Add an action for CLI option handler. Note that you must also provide
    /// function hint for this option handler. This hint will be shown if arguments
    /// passed to this handler are invalid, or when the CLI is calling the `--help`
    /// option. This kind of hint usually a single line of `String`, but it is not
    /// restricted for multi-line `String`.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// // code ...
    ///
    /// fn show_help(handler: &Handler) {
    ///     println!("This is help message!");
    /// }
    ///
    /// let mut handler: Handler = Handler::new();
    /// handler.add(
    ///     String::from("--help"),
    ///     Variant::Plain(show_help),
    ///     String::from("Show this help message.")
    /// );
    /// ```
    pub fn add(&mut self, parameter: String, action: Variant, hint: String) {
        match &mut self.actions {
            None => {
                let mut map: HashMap<String, Variant> = HashMap::new();
                map.insert(parameter.clone(), action);
                self.actions = Some(map);
            }
            Some(map) => {
                map.insert(parameter.clone(), action);
            }
        }
        match &mut self.hints {
            None => {
                let mut map: HashMap<String, String> = HashMap::new();
                map.insert(parameter, hint);
                self.hints = Some(map);
            }
            Some(map) => {
                map.insert(parameter, hint);
            }
        }
    }

    /// Append `Handler` instance passed to the calling `Handler` instance.
    ///
    /// # Example
    ///
    /// ```
    /// use medusa::{Handler, Variant};
    ///
    /// // code ...
    ///
    /// let mut handler: Handler = Handler::new();
    /// // code ...
    ///
    /// let mut other_handler: Handler = Handler::new();
    /// // code ...
    ///
    /// // append the first handler to the second
    /// other_handler.append(handler);
    /// ```
    pub fn append(&mut self, handler: Handler) {
        if let Some(target_actions) = handler.clone_actions() {
            if let Some(current_actions) = &mut self.actions {
                current_actions.extend(target_actions);
            }
        }
        if let Some(target_hints) = handler.extract_hints() {
            if let Some(current_hints) = &mut self.hints {
                current_hints.extend(target_hints);
            }
        }
    }
}
